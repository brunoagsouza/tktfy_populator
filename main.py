from environs import Env
import psycopg2

env = Env()

env.read_env()

user = env.str("POSTGRES_USER")
password = env.str("POSTGRES_PASSWORD")
db = env.str("POSTGRES_DB")
host = env.str("POSTGRES_HOST")
db_port = env.str("POSTGRES_PORT")

conn = psycopg2.connect(
    f"dbname = {db} user = {user} password = {password} host = {host} port = {db_port}")

cur = conn.cursor()

tickets_tickettype_data = (
    (1, "Inteira", 0, True, False, 1, "Inteira"),
    (2, "Meia Estudante", 50, True, False, 2, "Estudante"),
    (3, "Voucher", 100, False, False, 0, "Voucher"),
    (4, "Cortesia", 100, False, False, 0, "Cortesia"),
    (5, "Meia doador de Sangue", 50, True, False, 2, "Doador de Sangue"),
    (6, "Ingresso Promocional", 100, False, False, 0, "Ingresso Promocional"),
)

for ticket_type in tickets_tickettype_data:

    cur.execute("insert into tickets_tickettype (id, name,discount_percent, allowed_online, customize_category, display_order, description) values (%s,%s,%s,%s,%s,%s,%s)",
                (ticket_type))

group_user_data = (
    (1, "Supervisor"),
    (2, "Vendedor")
)

for group in group_user_data:

    cur.execute("insert into auth_group (id, name) values (%s,%s)", group)


conn.commit()
cur.close()
conn.close()
